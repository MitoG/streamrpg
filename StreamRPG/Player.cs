﻿using System.Collections.Generic;

namespace StreamRPG
{
    public class Player : Skills
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string NickName { get; private set; }
        public uint Age { get; private set; }

        public float Money { get; private set; }

        private float SpeakingSkill
        {
            get { return Speaking; }
            set { Speaking = value; }
        }
        private float CharismaSkill
        {
            get { return Charisma; }
            set { Charisma = value; }
        }
        private float EntertainmentSkill
        {
            get { return Entertainment; }
            set { Entertainment = value; }
        }

        private float ViewerChance { get; set; }

        public List<Channel> Channels { get; private set; }

        public bool IsStreaming = false;
        public bool isWorking = false;
        
        public Player(string firstname, string lastname, string nickname, uint age)
        {
            FirstName = firstname;
            LastName = lastname;
            NickName = nickname;
            Age = age;


            Money = 200.00f;

            SpeakingSkill = 0.001f;
            Channels = new List<Channel> {new Channel(NickName)};
        }

        public void Stream()
        {
            if (!IsStreaming)
            {
                Channels[0].Stream();
                IsStreaming = true;
            }
            else
            {
                UI.QueueString("You are already streaming!");
            }
        }

        public void Stream(string channel)
        {
            if (!IsStreaming)
            {
                Channels.Find(c => c.Name == channel).Stream();
                IsStreaming = true;
            }
            else
            {
                UI.QueueString("You are already streaming!");
            }
        }

        public void StopStream()
        {
            Channels.Find(c => c.IsOnline).StopStream();
            IsStreaming = false;
        }

        public void StartWork()
        {
            if (!isWorking)
                isWorking = true;
            else
                UI.QueueString("You are already working!");
        }

        public void StopWork()
        {
            if (isWorking)
                isWorking = false;
            else
                UI.QueueString("You are not working!");
        }

        public void SkillIncrease()
        {
            if (!IsStreaming) return;
            IncreaseSpeaking();
            IncreaseCharisma();
            IncreaseEntertainment();
        }

        private void IncreaseSpeaking()
        {
            if (!(rnd.NextDouble() >= IncreasementChanceSP)) return;
            SpeakingSkill += 0.1f;
            UI.QueueString("You increased your Speaking skill!");
        }

        private void IncreaseCharisma()
        {
            if (!(rnd.NextDouble() >= IncreasementChanceCHR)) return;
            Charisma += 0.1f;
            UI.QueueString("You increased your Charisma skill!");
        }

        private void IncreaseEntertainment()
        {
            if (!(rnd.NextDouble() >= IncreasementChanceENT)) return;
            EntertainmentSkill += 0.1f;
            UI.QueueString("You increased your Entertainment skill!");
        }

    }
}