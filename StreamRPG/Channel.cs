﻿using System;

namespace StreamRPG
{
    public class Channel
    {
        public string Name { get; set; }
        public uint Followers { get; set; }
        public uint Subs { get; set; }
        public uint Views { get; set; }
        public TimeSpan TotalStreamTime { get; set; }
        public bool IsPartner { get; set; }
        public bool IsOnline { get; private set; }
        public DateTime StreamStartTime { get; private set; }

        public Channel(string name)
        {
            Name = name;
            Followers = 0;
            Subs = 0;
            Views = 0;
            IsPartner = false;
            TotalStreamTime = new TimeSpan(0);
        }

        public void Stream()
        {
            IsOnline = true;
            StreamStartTime = Date.DateTime;
            UI.QueueString(Name + " just went online!");
        }

        public void CalculateStream()
        {
            
        }

        public void StopStream()
        {
            IsOnline = false;

            TimeSpan span = Date.DateTime - StreamStartTime;
            TotalStreamTime = TotalStreamTime.Add(span);
            UI.QueueString(String.Format(Name + " went offline after streaming for {0}:{1} Hours", span.Hours.ToString("00"), span.Minutes.ToString("00")));
        }
    }
}
