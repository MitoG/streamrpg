﻿using System;

namespace StreamRPG
{
    public static class Options
    {
        public static void Display()
        {
            GlobalVars.IsPaused = true;
            Console.WriteLine("**OPTIONS**");
            Console.WriteLine("'Speed+' to accelerate the Game");
            Console.WriteLine("'Speed-' to deccelerate the Game");

            string input = Console.ReadLine();

            if (input != null && input.ToLower() == "speed-")
            {
                GlobalVars.SetSpeed(--GlobalVars.Speed);
            }
            else if (input != null && input.ToLower() == "speed+")
            {
                GlobalVars.SetSpeed(++GlobalVars.Speed);
            }

            GlobalVars.IsPaused = false;
        }
    }
}
