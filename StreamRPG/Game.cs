﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace StreamRPG
{
    public enum GameType
    {
        None, // Only for Type2!!!
        Action,
        RPG,
        FPS,
        TPS, //Third Person Shooter
        Simulator,
        MMORPG,
        Horror,
        Survival
    }

    [XmlRoot("Games")]
    public class Games
    {
        [XmlRoot("Game")]
        public class Game
        {
            [XmlAttribute("Title")]
            public string Title { get; set; }

            [XmlAttribute("Hype")]
            public float Hype { get; set; }

            [XmlAttribute("Type")]
            public GameType Type { get; set; }

            [XmlAttribute("Type2")]
            public GameType Type2 { get; set; }

            [XmlAttribute("Price")]
            public float Price { get; set; }
        }

        [XmlArray("GameList")]
        [XmlArrayItem("Game")]
        public Game[] games { get; set; }
    }
}
