﻿using System;

namespace StreamRPG
{
    public class Skills
    {
        public float Speaking = 0.1f;
        public float Charisma = 0.1f;
        public float Entertainment = 0.1f;

        public float IncreasementChanceSP = 0.999f;
        public float IncreasementChanceCHR = 1f;
        public float IncreasementChanceENT = 1.5f;

        public static Random rnd = new Random();
    }
}
