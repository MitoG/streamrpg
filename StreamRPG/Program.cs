﻿namespace StreamRPG
{
    class Program
    {
        static void Main()
        {
            Init();

            while (GlobalVars.IsRunning)
            {
                Update();
                GlobalVars.Tick();
            }
        }

        static void Init()
        {
            StreamCom.Intro();
        }

        static void Update()
        {
            if (GlobalVars.Update)
            {
                Date.Tick();
                UI.Draw();
                GlobalVars.Player.SkillIncrease();
            }
        }
    }
}
