﻿using System;
using System.Runtime.CompilerServices;

namespace StreamRPG
{
    public class StreamCom
    {
        public static void Intro()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Stream.com, what would you like to do ?");
            Console.WriteLine("(1) Register new Account");
            Console.WriteLine("(2) Quit");

            StreamComIntroEnums selection;
            if (StreamComIntroEnums.TryParse(Console.ReadLine(), out selection))
            {
                switch (selection)
                {
                    case StreamComIntroEnums.Register:
                        Register();
                        break;

                    case StreamComIntroEnums.Quit:
                        GlobalVars.IsRunning = false;
                        break;

                    default:
                        Intro();
                        break;
                }   
            }
            else
            {
                Intro();
            }
            Console.Clear();
        }

        static void Register()
        {
// ReSharper disable JoinDeclarationAndInitializer
            string firstname;
            string lastname;
            string nickname;
            string answer;
            uint age;
// ReSharper restore JoinDeclarationAndInitializer

            Console.WriteLine("Register:");
            Console.Write("Fistname: ");
            firstname = Console.ReadLine().Replace("Firstname: ", "");
            Console.Write("Lastname: ");
            lastname = Console.ReadLine().Replace("Lastname: ", "");
            Console.Write("Nickname: ");
            nickname = Console.ReadLine().Replace("Nickname: ", "");
            Console.Write("Age: ");
            uint.TryParse(Console.ReadLine().Replace("Age: ", ""), out age);
            Console.Clear();

            firstname = firstname == "" ? "John" : firstname;
            lastname = lastname == "" ? "Doe" : lastname;
            nickname = nickname == "" ? "J.D." : nickname;
            age = age == 0 ? 23 : age;

            Console.WriteLine("Firstname: {0}\n" +
                              "Lastname: {1}\n" +
                              "Nickname: {2}\n" +
                              "Age: {3}", firstname, lastname, nickname, age);
            Console.Write("Are these informations correct ? [y]/n: ");
            answer = Console.ReadLine().Replace("Are these informations correct ? [y]/n: ", "");
            answer = answer == "" ? "y" : answer;

            if (answer.ToLower() == "y")
            {
                GlobalVars.Player = new Player(firstname, lastname, nickname, age);
            }
            else
            {
                Console.WriteLine("Starting over.");
                Console.Write("Press \"ENTER\" to continue..");
                Console.ReadLine();
                Register();
            }

        }
    }
}
