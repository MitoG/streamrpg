﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace StreamRPG
{
    public class GlobalVars
    {
        public static bool IsRunning = true;
        public static bool IsPaused = false;

        public static TimeSpan GameTime;

        public static bool Update = true;

        public static Player Player;
        public static List<Games.Game> GameList = new List<Games.Game>();

        private static readonly Stopwatch Stopwatch = new Stopwatch();

        public static uint Speed = 1;

        public static void Tick()
        {
            if (!IsPaused)
            {
                if (!Stopwatch.IsRunning)
                {
                    Update = false;
                    Stopwatch.Reset();
                    Stopwatch.Start();
                }
                if (Stopwatch.ElapsedMilliseconds >= (1000 / GlobalVars.Speed))
                {
                    Stopwatch.Stop();
                    Update = true;
                }    
            }
            while (IsPaused)
            {
                Console.WriteLine("Press 'ENTER' to continue!");
                Console.ReadLine();
                IsPaused = false;
            }
            
        }

        public static void SetSpeed(uint speed)
        {
            if (speed > 4)
            {
                Speed = 1;
            }
            else if(speed < 1)
            {
                Speed = 1;
            }
            else
            {
                Speed = speed;
            }
        }

        public static void ReadGamesXML()
        {
            string path = Directory.GetCurrentDirectory() + "\\games.xml";
            TextReader reader = new StreamReader(path);
            XmlSerializer ser = new XmlSerializer(typeof(Games), new XmlRootAttribute("Games"));

            Games games = (Games)ser.Deserialize(reader);
            GameList = games.games.ToList();

            GameList.ForEach(x => UI.QueueString(x.Title + " loaded!"));
        }
    }
}
