﻿using System;
using System.Globalization;

namespace StreamRPG
{
    public static class Date
    {
        public static DateTime DateTime = new DateTime(2015,01,05,12,00,00,00);

        public static void Tick()
        {
            DateTime = DateTime.AddMinutes(5);
        }

        public static string Display()
        {
            return DateTime.ToString("ddd dd MMM yyyy HH:mm", CultureInfo.InvariantCulture);
        }
    }
}
