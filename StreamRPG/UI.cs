﻿using System;
using System.Linq;

namespace StreamRPG
{
// ReSharper disable once InconsistentNaming
    public static class UI
    {
        private static readonly string[] PrintQueue = new string[24];

        public static void Draw()
        {
            Console.Clear();
            Console.WindowHeight = 28;
            Console.WindowWidth = 80;

            #region Top
            Console.SetCursorPosition(0, 0);
            Console.Write("Nickname: {0}  Money:{1}  Speed:{2}  ", GlobalVars.Player.NickName, GlobalVars.Player.Money, GlobalVars.Speed);
            if (GlobalVars.Player.Channels.Find(x => x.Name == GlobalVars.Player.NickName).IsOnline)
            {
                TimeSpan span = Date.DateTime -
                                GlobalVars.Player.Channels.Find(x => x.Name == GlobalVars.Player.NickName)
                                    .StreamStartTime;

                Console.Write("Channel: {0} is streaming since {1}:{2} Hours",
                               GlobalVars.Player.Channels.Find(x => x.Name == GlobalVars.Player.NickName).Name,span.Hours.ToString("00"), span.Minutes.ToString("00"));
            }
            Console.SetCursorPosition(0,1);
            Console.Write("********************************************************************************\n");
            //Console.SetCursorPosition(0,2);
            #endregion

            #region Body
            
            foreach (string s in PrintQueue.Where(s => s != null))
            {
                Console.Write(s + "\n");
            }

            #endregion

            #region Bottom
            Console.SetCursorPosition(0, 25);
            Console.WriteLine("********************************************************************************");
            Console.SetCursorPosition(0, 26);
            Console.Write("Date: {0}", Date.Display());
            Console.SetCursorPosition(0, 27);
            Console.Write(GlobalVars.Player.IsStreaming
                ? "Stop 'Stream'  'Watch' Stream  'Work'  'Options'  'Pause'  'Quit'"
                : "'Stream'  'Watch' Stream  'Work'  'Options'  'Pause'  'Quit'");
            Console.SetCursorPosition(0, 2);
            #endregion
            
            while (Console.KeyAvailable)
            {
                InputHandler.Handle(Console.ReadLine());
            }
        }

        public static void QueueString(string s)
        {
            //PrintQueue.Count(t => t != null)

            for (int i = PrintQueue.Length - 1; i > 0; i--)
            {
                PrintQueue[i] = PrintQueue[i - 1];
            }
            PrintQueue[0] = s;
        }
    }
}
