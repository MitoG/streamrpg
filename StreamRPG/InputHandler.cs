﻿using System;
using System.Threading;

namespace StreamRPG
{
    public static class InputHandler
    {
        private static Thread _worker;

        enum InputTypes
        {
            Save,
            Load,
            Options,
            Stream,
            Work,
            Buy,
            Learn,
            Watch,
            Quit,
            Pause
        }

        public static void Handle(string input)
        {
            InputTypes type;
            if (!Enum.TryParse(input, true, out type)) return;
            _worker = new Thread(InputWorker);
            _worker.Start(type);
        }

        private static void InputWorker(object input)
        {
            var type = (InputTypes) input;

            switch (type)
            {
                case InputTypes.Load:
                    GlobalVars.ReadGamesXML();
                    break;

                case InputTypes.Buy:
                case InputTypes.Learn:
                case InputTypes.Save:
                case InputTypes.Watch:
                    UI.QueueString("Not Implemented");
                    break;

                case InputTypes.Work:
                    if (!GlobalVars.Player.isWorking)
                        GlobalVars.Player.StartWork();
                    else
                        GlobalVars.Player.StopWork();
                    break;

                case InputTypes.Stream:
                    if (!GlobalVars.Player.IsStreaming)
                        GlobalVars.Player.Stream();
                    else
                        GlobalVars.Player.StopStream();
                    break;

                case InputTypes.Options:
                    Options.Display();
                    break;

                case InputTypes.Pause:
                    GlobalVars.IsPaused = !GlobalVars.IsPaused;
                    break;

                case InputTypes.Quit:
                    GlobalVars.IsRunning = false;
                    break;

                default:
                    break;
            }
        }
    }
}
